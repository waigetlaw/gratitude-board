import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import { LandingPage } from "./landing/landing";
import { Board } from "./board/board";
import dotenv from "dotenv";
dotenv.config();

class App extends React.Component {
    render() {
        return (
            <Router basename="/gratitude-board">
                <Route exact path="/" component={LandingPage} />
                <Route path={"/board/:boardName"} component={Board} />
            </Router>
        );
    }
}

export default App;
