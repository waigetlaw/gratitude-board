import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCopy } from "@fortawesome/free-solid-svg-icons";
import { CopyToClipboard } from "react-copy-to-clipboard";
import "./board.css";
import { setupIota, sendMessage } from "../utils/iota";
import { convertBoardNameToTag, convertBoardNameToSeed, getMessagesFromAccountTransfers, sortMessages, validateMessage } from "../utils/boardUtils";
import { Messages } from "./messages/messages";

export class Board extends React.Component {
    _isMounted = false;

    constructor(props) {
        super(props)
        this.state = {
            value: "",
            author: "",
            seed: convertBoardNameToSeed(props.match.params.boardName),
            iota: setupIota(),
            tag: convertBoardNameToTag(props.match.params.boardName),
            loading: true,
            sending: 0 // 0: not sending, 1: sending, 2: failed, 3: success, 4: validationFail
        }

        this.handleMessageChange = this.handleMessageChange.bind(this);
        this.handleMessageSubmit = this.handleMessageSubmit.bind(this);
        this.handleAuthorChange = this.handleAuthorChange.bind(this);
        this.updateMessages = this.updateMessages.bind(this);
        this.getLatestAddress = this.getLatestAddress.bind(this);
        this.handleBack = this.handleBack.bind(this);
    }

    async componentDidMount() {
        this._isMounted = true;
        await this.updateMessages();
        this.safeSetState({ ...this.state, loading: false });
    }

    safeSetState(state) {
        if (this._isMounted) {
            this.setState(state);
        }
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    handleMessageChange(event) {
        this.safeSetState({ ...this.state, value: event.target.value });
    }

    handleAuthorChange(event) {
        this.safeSetState({ ...this.state, author: event.target.value });
    }

    async updateMessages() {
        const accountData = await this.state.iota.getAccountData(this.state.seed);
        console.log("AccountData:\n", accountData);
        const messages = getMessagesFromAccountTransfers(accountData.transfers);
        this.safeSetState({ ...this.state, latestAddress: accountData.latestAddress, messages: sortMessages(messages) });
    }

    async getLatestAddress() {
        const latestAddress = this.state.latestAddress;
        if (latestAddress) {
            return latestAddress;
        }
        return (await this.state.iota.getAccountData(this.state.seed)).latestAddress;
    }

    setSendingState(i) {
        clearTimeout(this._errorFade);
        this.safeSetState({ ...this.state, sending: i });
        if (i !== 1) {
            this._errorFade = setTimeout(() => {
                this.safeSetState({ sending: 0 });
            }, 5000);
        }
    }

    async handleMessageSubmit() {
        if (!validateMessage(this.state.value)) {
            return this.setSendingState(4);
        }
        this.setSendingState(1);
        const latestAddress = await this.getLatestAddress();
        const message = JSON.stringify({ message: this.state.value, author: this.state.author });

        console.log(`Making transaction to ${latestAddress}`);
        console.log("Message:\n\n", message);
        try {
            const bundle = await sendMessage(this.state, latestAddress, message);
            console.log(`Bundle: ${JSON.stringify(bundle, null, 1)}`);
            this.setSendingState(3);
            this.updateMessages();
        } catch (e) {
            this.setSendingState(2);
            console.log(e);
        }
    }

    handleBack(event) {
        this.props.history.push("");
        event.preventDefault();
    }

    render() {
        const sendingMessage = [
            null,
            { type: "info", message: "Sending message to the IOTA Network..." },
            { type: "error", message: "Transaction failed, please try again." },
            { type: "success", message: "Transaction success!" },
            { type: "error", message: "Message cannot contain invalid characters or be blank. (Note: £ and € is considered an invalid char)" }
        ][this.state.sending];
        return (
            <div className="boardPage">
                <div className="board-background"></div>
                <div>
                    <div className="main disable-selection">
                        <div className="board-heading">
                            <span className="board-title">
                                {this.props.match.params.boardName}
                            </span><br />
                            <span className="tag">Tag: {this.state.tag}&nbsp;
                                <CopyToClipboard text={this.state.tag}>
                                    <FontAwesomeIcon className="copy-tag" icon={faCopy} />
                                </CopyToClipboard>
                            </span>
                        </div>
                        <button className="back-button" onClick={this.handleBack}>&lt;&lt; Back</button>
                    </div>
                    <p className="disable-selection">Check out all transactions on this board by searching for the tag or individual transaction hash at: <a href="https://devnet.thetangle.org/"
                        target="_blank" rel="noopener noreferrer">https://devnet.thetangle.org/</a></p><br /><hr /><br />
                    <span className="disable-selection">Write a new message:</span>
                    <textarea placeholder="Write your message here" className="textBox" maxLength="500" type="text" rows="10"
                        value={this.state.newMessage} onChange={this.handleMessageChange} />
                    <span className="disable-selection">Author:</span><br />
                    <div className="add-message-section">
                        <input className="author-input" maxLength="40" placeholder="Optional" value={this.state.author} onChange={this.handleAuthorChange} />
                        <button className="disable-selection submit-message" disabled={this.state.sending === 1} type="button" onClick={this.handleMessageSubmit}>Submit</button>
                    </div><br />
                    <div className="submit-info">
                        {sendingMessage ? <div className={"message-status " + sendingMessage.type}>{sendingMessage.message}</div> : null}
                    </div>
                </div>
                <hr />
                <div className="disable-selection message-title">Messages</div><hr /><br />
                <div className="message-section">{this.state.loading ? "Loading messages from IOTA Network..." : <Messages key="messages" messages={this.state.messages} />}</div>
            </div>
        );
    }
}
