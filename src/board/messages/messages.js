import React from "react";
import "./messages.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCopy } from "@fortawesome/free-solid-svg-icons";
import { CopyToClipboard } from "react-copy-to-clipboard";

export const Messages = (props) => {
    return (
        <div className="disable-selection messages">
            <h3>This Month:</h3>
            {props.messages.thisMonth.length === 0 ? "No messages to display." : ""}
            {props.messages.thisMonth.map(m => <Message key={m.timestamp} mes={m} />)}
            <hr />
            <h3>Previous Month:</h3>
            {props.messages.prevMonth.length === 0 ? "No messages to display." : ""}
            {props.messages.prevMonth.map(m => <Message key={m.timestamp} mes={m} />)}
            <hr />
            <h3>Older Messages:</h3>
            {props.messages.rest.length === 0 ? "No messages to display." : ""}
            {props.messages.rest.map(m => <Message key={m.timestamp} mes={m} />)}
            <hr />
        </div>
    )
}

function Message(props) {
    let renderedText = props.mes.message.split("\n").map((item, i) => {
        return <p className="message-content" key={i}>{item}</p>
    });
    return (
        <div className="message">
            <p className="message-time">Time: {new Date(props.mes.timestamp * 1000).toUTCString()}</p>
            {renderedText}
            <p className="message-content">- {props.mes.author ? props.mes.author : "anonymous"}</p>
            <hr className="message-hr" />
            <p className="wrapword message-hash">Transaction Hash:
            <CopyToClipboard text={props.mes.hash}>
                    <FontAwesomeIcon className="message-copy-tag" icon={faCopy} />
                </CopyToClipboard>
                <br /><br />
                {props.mes.hash}&nbsp;
            </p>
        </div>
    )
}
