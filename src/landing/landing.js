import React from "react";
import "./landing.css";
import { withRouter } from "react-router-dom";

class Landing extends React.Component {
    _isMounted = false;
    _errorFade;
    constructor(props) {
        super(props);
        this.state = { boardName: "" }

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.validateBoardName = this.validateBoardName.bind(this);
    }

    componentDidMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    handleChange(event) {
        this.setState({ boardName: event.target.value });
    }

    handleSubmit(event) {
        if (this.validateBoardName(this.state.boardName)) {
            this.props.history.push("board/" + this.state.boardName);
        } else {
            this._errorFade = setTimeout(() => {
                if (this._isMounted) {
                    this.setState({ message: undefined });
                }
            }, 5000);
        }
        event.preventDefault();
    }

    validateBoardName(boardName) {
        const validBoardLength = boardName && boardName.length && boardName.length < 21;
        const validCharacters = /^[a-zA-Z\s0-9\-_]+$/.test(boardName);
        if (!validBoardLength) {
            clearTimeout(this._errorFade);
            this.setState({
                boardName,
                message: "Error: Board name must be 1-20 characters long"
            });
            return false;
        }
        if (!validCharacters) {
            clearTimeout(this._errorFade);
            this.setState({
                boardName,
                message: "Error: Board name cannot have special characters except dashes and underscores"
            });
            return false;
        }
        return true;
    }

    render() {
        return (
            <div className="landing-root disable-selection">
                <div className="landing-background"></div>
                {this.state.message ? <Error error={this.state.message} /> : null}
                <form className="landing-form" onSubmit={this.handleSubmit}>
                    Board Name<br />
                    <input className="landing-input" spellCheck="false" type="text" value={this.state.boardName} onChange={this.handleChange} /><br />
                    <button className="landing-button" type="submit">Enter</button>
                </form>
                <h4 className="heroText">Say your thanks, and make it last forever!*</h4>
                <span className="small-print">
                    * Disclaimer: this app uses IOTA, <strong><em>cutting edge blockchain technology**</em></strong> for persistence. I hold no responsibility
                    for data loss if the IOTA network is to come down or from snapshotting.<br /><br />
                    ** IOTA is in fact not a blockchain but a "tangle" which is a directed acyclic graph (DAG) - don't worry, it's still cutting edge!</span>
                <div className="landing-footer-text">
                    Created by Wai Get Law<br />
                    View project at: <a href="https://gitlab.com/waigetlaw/gratitude-board" target="_blank" rel="noopener noreferrer">
                        https://gitlab.com/waigetlaw/gratitude-board
                        </a>
                </div>
            </div>
        );
    }
}

function Error(props) {
    return (
        <div className="landing-error">
            {props.error}
        </div>
    );
}

export const LandingPage = withRouter(Landing);
