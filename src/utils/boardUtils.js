import { sha256 } from "js-sha256";
import * as Converter from "@iota/converter";

export const convertBoardNameToTag = (boardName) => {
    let hash = sha256(boardName + process.env.REACT_APP_SECRET);
    let map = ["r", "s", "t", "u", "v", "w", "x", "y", "z"];
    map.forEach((letter, i) => hash = hash.replace(new RegExp(i, "g"), letter));
    return hash.substring(0, 27).toUpperCase();
}

export const convertBoardNameToSeed = (boardName) => {
    let hash = sha256(process.env.REACT_APP_SECRET + boardName);
    let map = ["i", "j", "k", "l", "m", "n", "o", "p", "q"];
    map.forEach((letter, i) => hash = hash.replace(new RegExp(i, "g"), letter));
    const times = Math.floor(81 / hash.length) + 1
    hash = hash.repeat(times);
    return hash.substring(0, 81).toUpperCase();
}

export const getMessagesFromAccountTransfers = (transfers) => {
    return transfers.map((transfer) => {
        let messageFragment = transfer[0].signatureMessageFragment;
        messageFragment = messageFragment.replace(/9*$/, "");
        messageFragment = messageFragment.length % 2 === 0 ? messageFragment : messageFragment + "9";
        try {
            return {
                ...JSON.parse(Converter.trytesToAscii(messageFragment).trim()),
                timestamp: transfer[0].timestamp,
                hash: transfer[0].hash
            }
        } catch (e) { }
        return null;
    }).filter(e => !!e).sort((a, b) => b.timestamp - a.timestamp);
}

export const sortMessages = (messages) => {
    const currentMonth = new Date().getMonth();
    const currentYear = new Date().getFullYear();
    const thisMonth = [], prevMonth = [], rest = [];
    messages.forEach((message) => {

        const messageMonth = new Date(message.timestamp * 1000).getMonth();
        const messageYear = new Date(message.timestamp * 1000).getFullYear();

        if (messageMonth === currentMonth && messageYear === currentYear) {
            thisMonth.push(message);
        }
        else if (messageMonth === ((currentMonth + 11) % 12) && messageYear === currentYear) {
            prevMonth.push(message);
        }
        else {
            rest.push(message);
        }
    });
    return { thisMonth, prevMonth, rest };
}

export const validateMessage = (message) => {
    return /[-a-zA-Z0-9!"$%^&*()-_=+[{\]};:'@#~,<.>/?\s|\\]/.test(message) && message.length;
}
