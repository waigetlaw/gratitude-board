import { composeAPI } from "@iota/core";
import * as Converter from "@iota/converter";

export const setupIota = () => {
    const iota = composeAPI({
        provider: process.env.REACT_APP_IOTA_NODE
    })

    iota.getNodeInfo()
        .then(info => console.log("IOTA Node info:\n", info))
        .catch(error => {
            console.log(`Request error: ${error.message}`)
        })

    return iota;
}

export const sendMessage = async ({ iota, seed, tag }, address, message) => {
    const transfers = [
        {
            value: 0,
            tag,
            address,
            message: Converter.asciiToTrytes(message)
        }
    ]
    const trytes = await iota.prepareTransfers(seed, transfers);
    return iota.sendTrytes(trytes, 3/*depth*/, 15/*minimum weight magnitude*/);
}
